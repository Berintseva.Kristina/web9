$(document).on("ready", function() {
  $('.slid').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
$(".slid2").slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	dots: false,
	arrows: false,
	responsive: [{
		breakpoint: 768,
		settings: {
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			dots: false,
			arrows: false
		}
	}, ]
});

});
